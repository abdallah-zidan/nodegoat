# Overview

The Nodegoat project provides an environment to learn how OWASP Top 10 security risks apply to web applications developed using Node.js and how to effectively address them. The original repository can be found here https://github.com/OWASP/NodeGoat

This modified repository will help guide you in setting up the Nodegoat application in Heroku leveraging MongoDB for the database and GitLab as the automated deployment mechanism. Nodegoat All the steps outlined below are meant to be completed in the order they are presented. There are images with most steps to help guide you along the way.

# Steps

   1) Create the MongoDB Cluster and Database
      1) Create an account for MongoDB
      2) Create a Project
      3) Create a Free Tier Cluster in the Project
      4) Create a Database user and give it AtlasAdmin rights
      5) Add Network Access rules for the Cluster
      6) Get the connection string for your Database Cluster
   2) Create the apps in Heroku
      1) Create an account for Heroku
      2) Create an app in Heroku
	  3) Configure a Variable for the app
	  4) Repeat the steps two more times for TEST and PROD
   3) Create a GitLab project and run the pipeline
      1) Create a GitLab account
      2) Clone this repository to a GitLab project
      3) Create GitLab Variables
      4) Run the pipeline
   4) Testing
      1) Connecting to the Nodegoat application hosted by Heroku
      2) Logging into Nodegoat

# How to Set Up NodeGoat with MondoDB, Heroku, and GitLab
## 1. Create the MongoDB Cluster and Database
   1) Create an account for MongoDB, if you don't already have one, at https://www.mongodb.com/cloud/atlas/signup
   2) Create a Project
      1) Click on New Project in the upper right
      
      ![Image of New Project Steps - 1](images/mongodb_new_proj_1.png)

      2) Give your Project a name, then click Next

      ![Image of New Project Steps - 2](images/mongodb_new_proj_2.png)

      3) Click Create Project

      ![Image of New Project Steps - 3](images/mongodb_new_proj_3.png)
      
   3) Create a Free Tier Cluster in the Project
      1) Click Build a Cluster
      
      ![Image of New Cluster Steps - 1](images/mongodb_new_clust_1.png)

      2) Under the free tier section, click Create a Cluster

      ![Image of New Cluster Steps - 2](images/mongodb_new_clust_2.png)

      3) Modify your region as needed, then click Create Cluster

      ![Image of New Cluster Steps - 3](images/mongodb_new_clust_3.png)

   4) Create a Database user and give it AtlasAdmin rights (needed to create, allocate, and access the database)
      1) Click Database Access from the list on the left
      
      ![Image of New DB User Steps - 1](images/mongodb_db_access_1.png)

      2) Click Add New Database User 

      ![Image of New DB User Steps - 2](images/mongodb_db_access_2.png)

      3) Input a username and password and change the Database User Privileges to AtlasAdmin
         - **NOTE** - If you use special characters in the password, they must be URL encoded later


      ![Image of New DB User Steps - 3](images/mongodb_db_access_3.png)
      
   5) Add Network Access rules for the Cluster
      1) Click Network Access from the list on the left
      
      ![Image of New Network Rule Steps - 1](images/mongodb_net_access_1.png)

      2) Click Add IP Address

      ![Image of New Network Rule Steps - 2](images/mongodb_net_access_2.png)

      3) Input the CIDR range of 0.0.0.0/0 and optionally input a comment

      ![Image of New Network Rule Steps - 3](images/mongodb_net_access_3.png)
      
   6) Get the connection string for your Database Cluster
      1) On the Clusters tab, under your cluster, click Connect
      
      ![Image of Connection Strings Steps - 1](images/mongodb_conn_strings_1.png)

      2) Select Connect Your Application

      ![Image of Connection Strings Steps - 2](images/mongodb_conn_strings_2.png)

      3) Select Node.js as the Driver, and the version of 2.2.12 or later or Python as the Driver, and the version of 3.11. Copy the string and save for later use

      ![Image of Connection Strings Steps - 3](images/mongodb_conn_strings_3.png)

## 2. Create the apps in Heroku
   1) Create an account for Heroku, if you don't already have one, at https://signup.heroku.com/
   2) Create an app in Heroku.
      1) In the top right, click New > Create New App
      
      ![Image of Heroku Setup Steps - 1](images/heroku_app_setup_1.png)

      2) Input a name for the app, then click Create App (You will need these names later). Repeat steps 1 and 2 two more times to create a test and production app

      ![Image of Heroku Setup Steps - 2](images/heroku_app_setup_2.png)
	
   3) Configure a Variable for the app
      1) After creating the app, click Settings in the top right
	  
      ![Image of Heroku Setup Steps - 3](images/heroku_app_setup_3.png)

      2) Click Reveal Config Vars in the Config Vars section
	  
      ![Image of Heroku Setup Steps - 4](images/heroku_app_setup_4.png)

      3) Input MONGODB_URI for the Key and the connect string that was copied earlier from MongoDB for the Value, then click Add
         - **NOTE**: Replace values, such as the password and database, for your environment (default database is MyFirstDatabase, change it to nodegoat)
	  
      ![Image of Heroku Setup Steps - 5](images/heroku_app_setup_5.png)

   4) Repeat steps 2 and 3 two more times to create a TEST and PROD Heroku App

## 3. Create a GitLab project and run the pipeline
   1) Create a GitLab account, if not done already, at https://gitlab.com/users/sign_up
   2) Clone this repository to a GitLab project
      1) On the GitLab home page, in the upper right, click New Project
      
      ![Image of GitLab Repo Setup Steps - 1](images/gitlab_new_proj_1.png)

      2) Select Import project

      ![Image of GitLab Repo Setup Steps - 2](images/gitlab_new_proj_2.png)

      3) Select Repo by URL and input https://gitlab.com/cmg_public/webinars/devops-crash-course/nodegoat.git for the Git repository URL, then click Create Project in the bottom left

      ![Image of GitLab Repo Setup Steps - 3](images/gitlab_new_proj_3.png)

   3) Create the following GitLab Variables under Settings > CI/CD > Variables:
         - HEROKU_API_KEY (The API Key that was created in Heroku)
            - In the top right, click your account icon > Account Settings
      
            ![Image of GitLab Variables Setup Steps - 1](images/heroku_api_key_1.png)

            - Scroll down and select Reveal for your API Key. Use this for the environment variable

            ![Image of GitLab Variables Setup Steps - 2](images/heroku_api_key_2.png)
            
         - HEROKU_APP_PRODUCTION (The name of the Production app that was created in Heroku)
         - HEROKU_APP_DEV (The name of the Dev app that was created in Heroku)
         - HEROKU_APP_TEST (The name of the Test app that was created in Heroku)
         - MONGODB_URI (The connection string generated in MongoDB - Copied from earlier)
            - **NOTE**: Replace values, such as the password and database, for your environment (default database is MyFirstDatabase, change it to nodegoat)
         - NODEGOAT_ADMIN_PASS (The password that will be assigned to the Admin login account for NodeGoat. This can be anything you want.)
            - **NOTE**: Do not use a '$' in your password. When this gets used in the pipeline, it, and any characters after it, will get stripped from the password

         - Your Variables should look similar to the following:

         ![Image of GitLab Variables Setup Steps - 2](images/gitlab_variables_1.png)

   4) Run the pipeline
      1) Click CI/CD > Pipelines
      
            ![Image of Pipeline Startup Steps - 1](images/gitlab_start_pipeline_1.png)

      2) Click Run Pipeline on both screens, no changes need to be made at this point
      
            ![Image of Pipeline Startup Steps - 2](images/gitlab_start_pipeline_2.png)
      
            ![Image of Pipeline Startup Steps - 3](images/gitlab_start_pipeline_3.png)


## 4. Testing

   1) Connecting to the Nodegoat application hosted by Heroku
      1) You should be able to easily connect to the application using the following format, replacing <Heroku_App_Name> with the name of your application in Heroku:
         - https://<Heroku_App_Name>.herokuapp.com/
   2) Logging into Nodegoat
      1) Once you have connected to the application, you should be able to login with the following credentials:
         - Username: Admin
         - Password: Value input for GitLab variable 'NODEGOAT_ADMIN_PASS'


# Summary

If you successfully setup Nodegoat on Heroku using MongoDB and GitLab, congratulations! If you are having issues, go back through the steps and make sure that everything is configured correctly. You should now have a fully functional CI/CD pipeline for nodegoat that you can further modify if desired. 

# Next Steps

Check out the [Tutorial Guide](http://nodegoat.herokuapp.com/tutorial) explaining how each of the OWASP Top 10 vulnerabilities can manifest in Node.js web apps and how to prevent it.

Additionally, you should have a decent understanding of Heroku, GitLab, and MongoDB at this point to try and build out some more automation pipelines, possibly with your own code.
