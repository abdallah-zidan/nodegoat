from pymongo import MongoClient
import pymongo
import os

def get_database():
    
    # Provide the mongodb atlas url to connect python to mongodb using pymongo
    connection_string = os.environ.get('MONGODB_URI')

    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = MongoClient(connection_string)

    # Create the database for our example (we will use the same database throughout the tutorial
    return client['nodegoat']
 
# Get the database
dbname = get_database()
nodegoat_admin_pass = os.environ.get('NODEGOAT_ADMIN_PASS')

allocations_col = dbname["allocations"]
users_col = dbname["users"]

try:
    dbname.create_collection('contributions')
    dbname.create_collection('counters')
    dbname.create_collection('memos')
except pymongo.errors.CollectionInvalid:
    print("Collections already exist")

try:
    allocations_col.insert_many(
        [
            {
                "_id": "6086855fdc52ab1ae9ab8d08",
                "userId": 2,
                "stocks": 50,
                "funds": 100,
                "bonds": 100
            },
            {
                "_id": "60868570dc52ab1ae9ab9b00",
                "userId": 3,
                "stocks": 50,
                "funds": 100,
                "bonds": 100
            },
            {
                "_id": "608685b5dc52ab1ae9abf75b",
                "userId": 1,
                "stocks": 50,
                "funds": 100,
                "bonds": 100
            }
        ]
    )
except pymongo.errors.BulkWriteError:
    print("Values already exists in Collection")

try:
    users_col.insert_one(
        {
            "_id": 1,
            "userName": "Admin",
            "firstName": "Administrator",
            "lastName": "Administrator",
            "benefitStartDate":"2020-01-10",
            "password": nodegoat_admin_pass,
            "isAdmin":True
        }
    )
except pymongo.errors.DuplicateKeyError:
    print("Value already exists in Collection")
